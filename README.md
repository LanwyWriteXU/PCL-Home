# PCL Ordinary Home

## 普通的PCL主页

### 主页预览

![这是头图](https://raw.gitcode.com/LanwyWriteXU/PCL-Home/attachment/uploads/4624204a-c7ec-42a4-91de-60d1339c5e1e/5e0d4259-cf94-4e4d-9bad-f3d70f7c2054.png '这是头图')
这是头图

包含多种功能，别人有的我也有，别人没有的我也有！

而且，开源！

### 怎么使用

将链接 `https://raw.gitcode.com/LanwyWriteXU/PCL-Home/raw/main/Custom.xaml` 填入“自定义联网更新”中就可以获取主页了。

如果想要在自己的主页加载我的主页，可以在组件的`EventType`属性填入“`打开帮助`”，并且在`EventData`填入` https://raw.gitcode.com/LanwyWriteXU/PCL-Home/raw/main/Custom.json`，这样就可以加载我的普通主页啦！

### 请我喝奶茶

点[这里](https://afdian.com/a/cyberneko "https://afdian.com/a/cyberneko")，请我喝杯奶茶qwq